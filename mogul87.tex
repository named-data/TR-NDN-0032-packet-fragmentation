% \section{A Brief Summary of 1987 Paper}
\section{A brief Summary of ``Fragmentation Considered Harmful''}
\label{sec:mogul}

The original IP architecture design~\cite{RFC791} stated that IP packet fragmentation can be performed by any IP node as needed, and reassembly is done by the end receiving nodes only.   
The paper by Kent and Mogul ``Fragmentation Considered Harmful''~\cite{IPfragment}, published in SIGCOMM 1987, is the first study on the impact of fragmentation and reassembly.  
The presented argument is that IP packet fragmentation is ``at best a necessary evil; it can lead to poor performance or complete communication failure'', suggesting that one should look into a variety of ways to reduce the likelihood of fragmentation.  

\subsection{Why Harmful?}

Kent and Mogul stated two major performance costs of IP packet fragmentation~\cite{IPfragment}.
First, fragmentation leads to inefficient use of resources:
\begin{itemize}
\item Not only the router performing fragmentation incurs an extra computation cost, intermediate gateways must also forward more packets.  
  Furthermore, the receiving host must reassemble the fragments.
\item Additional headers consume additional bandwidth.
\item Poor choice of fragment sizes can greatly increase the cost of delivering a datagram. 
  A pathological case of TCP implementation at the time sent \emph{ten} 1024-byte TCP segments through ARPAnet which had a MTU size of 1006 bytes, resulting in 20 fragments instead of the source host sending 11 966-byte TCP segments.
\end{itemize}

Second, a single fragment loss requires the higher level protocol to retransmit all of the data in the original datagram, even if most of the fragments were received correctly.
A popular at the time Proteon network interface did not have sufficient interface buffering and always dropped the second packet of two back-to-back packets, resulting in unnecessary TCP or application-level retransmissions.

The paper also discussed the difficulties in efficient reassembly, due to inadequate information about packet fragments carried in IP header (e.g., how many fragments total) and arrival delays.

\subsection{Recommendations}

It is a common perception that Kent and Mogul~\cite{IPfragment} recommended avoiding packet fragmentation inside the network and performing fragmentation at source hosts.  
In fact IPv6~\cite{RFC2460} allows only fragmentation by source hosts.

However, a careful reading of the paper shows that the paper sorted its recommendations into three categories:
\begin{enumerate}
\item Recommendations not involving protocol changes;
\item Recommendations for protocol changes, and
\item Recommendations for new architectures.
\end{enumerate}

The authors viewed avoiding changes as a high priority consideration.
If we ignore this factor, we can also divide the recommendations into two classes: 
\begin{enumerate}
\item avoidance of network fragmentation with host reassembly, and
\item encouragement of ``transparent fragmentation'', or \emph{immediate reassembly}.\footnote{Historically, there were large scale subnets like ARPAnet, ``transparent fragmentation'' means fragmentation within a subnet.  If a subnet is limited to a single-hop (e.g., WiFi, Ethernet), \emph{immediate reassembly} means hop-by-hop fragmentation and reassembly.}
\end{enumerate}

Note that the first recommendation can be realized by either fragmentation only by sources or by fully implementing the second recommendation for ``transparent'' fragmentation.
In particular, the authors stated that
\begin{quote}
\emph{We urge consideration of transparent fragmentation whenever possible. There is little value in the ability to send fragments of one datagram along different routes, and reassembly by gateways should not be prohibitively expensive. Main memory sizes and costs are improving so rapidly that buffer space should no longer be considered the limiting resource; reassembly might actually improve the switching rates of gateways by reducing the number of individually switched fragments.}
\end{quote}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "fragment"
%%% End:
