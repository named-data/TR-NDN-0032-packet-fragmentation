# change this to the file name of the paper 
PRJ = fragment

DVIOPT = -P cmz -t letter -G0

all : ${PRJ}.pdf 

#${PRJ}.dvi : *.tex *.bib 
#	latex ${PRJ}; bibtex ${PRJ}; latex ${PRJ}; latex ${PRJ}
${PRJ}.pdf : *.tex *.bib 
	pdflatex ${PRJ} && (ls bu*.aux | xargs -n 1 bibtex) ; bibtex ${PRJ}.aux ; pdflatex ${PRJ} ; pdflatex ${PRJ} 

view : ${PRJ}.pdf
	open ${PRJ}.pdf &

# change this to include what should be in the tarball,  use --exclude for exclusion.
pack : clean
	cd ..; /bin/rm -f ${PRJ}.tgz; tar -hzcf ${PRJ}.tgz ${PRJ} 

clean:
	/bin/rm -f *.toc *.aux ${PRJ}.bbl *.blg *.log *.dvi *~* *.bak *.out *.synctex.gz

distclean: clean
	/bin/rm -f ${PRJ}.dvi ${PRJ}.ps ${PRJ}.pdf

