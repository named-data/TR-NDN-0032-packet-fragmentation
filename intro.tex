\section{Introduction}

The Internet is made of many heterogeneous networks and the level of this heterogeneity is continuously growing.
Constant increase of mobile networks, continuous penetration of the Internet connectivity into homes and vehicles, connecting remote villages and even oceans (underwater networking) all result in computing devices to be connected by all kinds of means, ranging from sonar to bluetooth to fiber.
One consequence of the heterogeneity in link-layer technology is the different maximum transmission unit (MTU) sizes across different links; any new Internet architecture needs to take this factor into consideration so that applications can work across networks with different links and continue to work as link layer technologies evolve.

Named Data Networking (NDN) architecture (\cite{ndn-base, ndn-tr, ndn-ccr}) makes application data the focus of communication.
This switch brings multiple advantages, including data-oriented security, built-in multicast capability, ability to cache application data and retrieve cached data regardless of the application type.
At the same time, heterogeneous link-layer technology brings up a question for the architecture design: whether applications need to fit their data into some minimum MTU size, or the network should implement some form of fragmentation and reassembly.

%LW: I added and revised the remaining paragraphs in this section.  Someone should take a look.
While applications should be aware of the prevalent MTU sizes in the network so that their data sizes are not too big or too small to seriously affect application performance, we believe that \textbf{application data units should not be constrained by the smallest possible MTU size among all networks} for the following reasons.  First, if an application does segments its data this way, it can be very inefficient if its packets do not actually traverse links with the smallest MTU.  Second, if most of the links have a higher MTU, constraining the application data unit to one or two links with a small MTU can also be quite inefficient.   

If we do not require applications to segment their data in order to fit some minimum MTU size, the question is then how to handle those packets that exceed the link MTU size.  There are several options for fragmenting and reassembling packets, the first two of which have been adopted in IP networks:
\begin{enumerate}
\item \emph{Hop-by-Hop Fragmentation with End-Host Reassembly} or simply \emph{\textbf{IPv4-style Fragmentation/Reassembly}}: in IPv4~\cite{RFC791}, each hop (re)fragments the packet as needed to fit its MTU size.  The fragments are not immediately assembled by the next hop, but instead are assembled at the final destination;
\item  \emph{End-to-End Fragmentation and Reassembly} or simply \emph{\textbf{IPv6-style Fragmentation/Reassembly}}: in IPv6~\cite{RFC2460}, the IP protocol at the source first discovers the smallest MTU over the path to the destination (PMTU), and then fragments data from higher layers to fit the PMTU.  The packets are not supposed to be further re-fragmented en-route and will be re-assembled at the destination;
\item \emph{\textbf{Hop-by-Hop Fragmentation and Reassembly}}: each hop fragments a packet as needed and the next hop assembles the packet immediately.  
\end{enumerate}
 
We believe that IPv4-style fragmentation/Assembly is not a viable option for NDN, given that:
(1) interest packets cannot be partially processed, as routers need to know the whole question in order to answer it;
(2) routers have to have a full data packet in order to match it against pending interests; and
(3) caches should hold full data packets, as the same data can be requested by multiple clients with disparate MTU sizes.
Moreover, IPv6-style fragmentation is not viable for NDN either, because the concept of Path MTU does not apply to NDN due to caching and asynchronous consumption of data and therefore producers cannot fragment or properly presize data packets.  

Due to the above reasons (see Section~\ref{sec:whyHBH} for more detailed explanation), NDN chooses to perform fragmentation and reassembly on a hop-by-hop basis (Option 3 above), i.e., when a packet cannot be delivered through a link due to MTU constraints, it will be fragmented and then immediately re-assembled by the next hop router for further processing. 

Strictly speaking, NDN's fragmentation and reassembly functionality is performed by a shim layer below the network layer of NDN when required\footnote{If NDN is run over a TCP or UDP tunnel, it does not need to handle fragmentation or reassembly since the TCP/UDP packets will be fragmented and reassembled by IP over the tunnel when necessary.}, which means that the network layer is unaware of and does not deal with fragments at all.  

Note that it is infeasible in general for IP to do reassembly at the immediate next hop, since IP may deliver different fragments of the same IP packet over different paths.  The consequence is that if any of the fragments is lost, the other fragments will still be delivered by the network instead of being discarded at the next hop, which leads to unnecessary consumption of network resources.  

In theory, IP networks could also use a shim layer below IP to perform fragmentation and reassembly as NDN does, but the concern is that the same packet may potentially suffer from repeated fragmentation and reassembly along the path.  This has also been an argument against HBH fragmentation and reassembly in NDN.

% LW: not sure if we need this
%\footnote{While applications are relaxed in the choice of data unit size, they need to balance between unit size and performance impact.} 

We argue that HBH fragmentation and reassembly in NDN not only ensures the correctness of interest and data packet delivery, but also brings a number of advantages (Section~\ref{sec:whyHBH}), including efficient handling of fragment losses and efficient utilization of large MTU sizes when only a few links in the middle have constrained MTU.  In fact, ``transparent" (i.e., HBH) fragmentation and reassembly was also recommended by Kent and Mogul in their seminal paper~\cite{IPfragment}, often (mis)cited as a justification for E2E-based approach.

In the rest of this memo, we first overview Kent and Mogul's findings and conclusions in ``Fragmentation Considered Harmful'' paper~\cite{IPfragment}, and then discuss in more detail the reasoning behind NDN's decision of HBH fragmentation and reassembly.

% In this memo we elaborate the pros and cons of placing fragmentation/reassembly at different places to explain our design decision.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "fragment"
%%% End:
