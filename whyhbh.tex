\section{Hop-by-Hop Fragmentation and Reassembly (HBH F/R) in NDN}
\label{sec:whyHBH}

In this section we first elaborate why we consider HBH to be the only viable option for fragmentation and reassembly in NDN architecture.
Afterwards, we discuss advantages and potential issues of application of HBH F/R in NDN.

\subsection{Why NDN Should Have Only HBH F/R?}

% (1) interest packets cannot be partially processed, as routers need to know the whole question in order to answer it;
% (2) routers have to have a full data packet in order to match it against pending interests; and
% (3) caches should hold full data packets, as the same data can be requested by multiple clients with disparate MTU sizes.

\subsubsection{Interest packets cannot be partially processed}

% First and foremost, 
NDN is an architecture specifically focused on data and data retrieval.
Consumers throw questions to the network for the desired data (express interests) and the network is responsible for finding and returning the requested data.
These interests are evaluated by routers on the path, each deciding whether the interests can be satisfied with previously cached data, or they should be aggregated with similar interests from another consumers, or further forwarded towards the anticipated location of data or data producer.
In this model, to make a decision, the router must have the full interest available, which is only possible if the interest is not fragmented.  Therefore, transparent HBH fragmentation should be used for interests.

\subsubsection{Full data packet is necessary to match pending interests}

To deliver data packets back to the requesters, NDN relies on the state created by the forwarded interests (``pending interests'').
More specifically, when a data packet is received, a router tries to match it against one or multiple pending interests.
This match is based on the data name and in many cases requires other parts of the data packet.
The best example here is retrieval of data packets using the implicit digest~\cite{ndn-tlv}.
If interest includes the implicit digest, routers are required to calculate the cryptographic digest over the complete data packet in order to match pending interest to the incoming data packet.
Therefore, each router along the path has to have a fully assembled data packet, before it is possible to make a complete decision about this packet.
By the same token, \emph{caches should also hold full data packets for correct matching of future interests}. 

\subsubsection{Producers cannot fragment or properly pre-size data packets (``Path MTU'' concept does not exist in NDN)}

Because of in-network storage, data in NDN is no longer necessarily retrieved from the original producer.  
As an example, it is likely that data in popular streaming videos, which makes a big portion of today's network traffic, may come from in-network storage.
% if users' interests continue to follow exponential distribution patterns.
Since the data packets can be cached at routers and get pulled down by any future consumers, the traditional ``end-to-end path'' concept no longer applies, nor does the ``path MTU'' concept.  Therefore, producers cannot presize data packets to avoid fragmentation, as it is impossible for them to know the MTU of all the future paths their data may traverse, 

NDN enables any node to communicate with any other node \emph{directly} as soon as and as long as any physical communication channel exists in between, without a prior configuration.  
For example, a car on the road could retrieve a morning news video file from a combined use of ad hoc connectivity (e.g., when vehicles meet on the road or at a gas station), DTN (e.g., data carried by another car), or paid cellular channels that can be the last resort for some pieces it cannot find from other cars. 
Therefore, different pieces of the same video file may come from different vehicles, again making it impossible for data producers to know the proper MTU size over all links. 
Even if it knew, packaging data with the smallest MTU would be a poor decision as it leads to unnecessary performance hit for data delivery to end consumers whose connectivity supports larger MTU sizes.

% The combined effects of the above two factors state that, if one cannot do ``path MTU discovery'', then in-network fragmentation is likely. 
% Now the choice is between whether doing immediate reassembly or letting fragments reach consumers and get resembled there, as IP does.  
% We note that an Interest packet, generally speaking, is likely smaller than the data packet it retrieves, yet one Interest packet can only retrieve one data packet in order to keep flow balancing.
% This dictates that (data) packets be assembled at next hop.  
% We also note that each data packet carries a signature, letting fragments flow through network means that none of the nodes in the middle would be able to verify the signature even when they want to and are able to.

\subsubsection{What about cut-through forwarding?}

%LW: I added this, someone needs to take a look
To avoid the delay incurred by repeated fragmentation and reassembly of an NDN data packet over a path, one proposal is to forward each fragment as soon as it is received and reassemble the fragments at the consumer (``cut-through forwarding")~\cite{secureseg}.  This is essentially IPv4-style F/R, except that it is much more complicated to implement in NDN.  Since a fundamental component of the NDN architecture is that each data packet carries a signature for verifying its authenticity, an efficient mechanism must be provided to verify the authenticity of each fragment without waiting for all the fragments to arrive.  Otherwise, one cannot get both the security benefit provided by the data signature and the lower-delay benefit of cut-through forwarding.  Although Ghali et. al.~\cite{secureseg} proposed an incremental authentication mechanism, their evaluation results show that this mechanism is quite slow in software implementation.  Moreover, since the matching between interests and data is primarily based on data names, every fragment (not just the first one) needs to carry the full data name.  In addition, interest matching can also be based on additional fields in a data packet, which is difficult (and perhaps impossible) to handle with fragments since each fragment contains only part of the data in the original packet.  Given these reasons, we prefer the simplicity of the HBH F/R approach over this option.


\subsection{Benefits}

HBH F/R not only ensures the correct delivery of interest and data packets as explained above, but also provides multiple advantages compared to alternative approaches.
First, proper implementation of HBH F/R protocol (e.g., NDNLP~\cite{ndnlp}) can efficiently recover from lost or corrupted fragments locally,
% without requiring the application-level retransmission.
which reduces packet delivery failures that require application-level retransmissions and avoids the inefficiency problem in IPv4-style F/R that one missing fragment results in the unnecessary transmission of all the remaining fragments.  
% LW: in IPv4 and IPv6 style F/R are also transparent to the consumer and producer application.
% given HBH F/R is completely transparent to both consumer and producer, applications have much more freedom in selecting proper sizes for data packets.
Second, given that missing fragments can be efficiently recovered, applications can make use of larger MTU sizes enabled by advanced link-layer networking technologies, and consider their own cost tradeoff in deciding data packet size, without worrying about confining the packet size to the smallest denominator MTU along the data retrieval path.  

\subsection{Discussion}

% Issues
% Repeated fragmentation  can be minimized by careful selection of segment sizes
% we still may need some MTU measuring/guessing mechanism, so higher level protocols segment data in reasonable chunks
Applications may desire to send large packets, say to reduce the signing cost, but one should also keep in mind that using a large packet size can increase the store-and-forward delay across every hop, an intrinsic factor in packet-switched networks
%\footnote{Cut-through forwarding can avoid this concern, at the cost of increased forwarding complexity.}.  
However, whether the packet size would lead to noticeable store-and-forward delay depends on the network speed.  A 1MB packet crossing a 1Gbps link has a store-and-forward delay of 8msec, but this delay is reduced to 0.8msec at 10Gbps, which is negligible compared to propagation delay.

HBH F/R does not mean that all producers can just make data packets arbitrarily big.
One still needs to carefully choose packet size with a well engineered value based on common MTU sizes in use, in order to avoid a packet being fragmented and reassembled multiple times along the retrieval path.

% NDN network performs hop-by-hop fragmentation and reassembly whenever necessary.
% Assuming nodes $N_{before}$ and $N_{after}$ are NDN nodes at the two ends of a link $L_s$, which has a MTU size smaller than the size of packet $P_{big}$ that $N_{before}$ needs to send to $N_{after}$.  $N_{before}$ will fragment $P_{big}$ and $N_{after}$ will reassemble $P_{big}$ back to the original form.
% The following fundamental reasons lead us to this HBH design decision.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "fragment"
%%% End:
